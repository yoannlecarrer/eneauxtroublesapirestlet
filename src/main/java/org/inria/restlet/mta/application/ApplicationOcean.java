package org.inria.restlet.mta.application;

import org.inria.restlet.mta.resources.SharkRessource;
import org.inria.restlet.mta.resources.SharksRessource;
import org.inria.restlet.mta.resources.TunasRessource;
import org.inria.restlet.mta.resources.ZoneRessource;
import org.restlet.Application;
import org.restlet.Context;
import org.restlet.Restlet;
import org.restlet.routing.Router;

/**
*
* ApplicationOcean.
*
* @author Yoann Le Carrer
* @author Lamine Sadi
* 
*/

public class ApplicationOcean extends Application
{

    public ApplicationOcean(Context context)
    {
        super(context);
    }

    @Override
    public Restlet createInboundRoot()
    {
        Router router = new Router(getContext());
        router.attach("/zones/{zone_id}", ZoneRessource.class);
        router.attach("/sharks", SharksRessource.class);
        router.attach("/sharks/{sharks_id}", SharkRessource.class);
        router.attach("/tunas", TunasRessource.class);
        return router;
    }
}
