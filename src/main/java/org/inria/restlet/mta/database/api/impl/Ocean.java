package org.inria.restlet.mta.database.api.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.inria.restlet.mta.backend.Poissons_Pilotes;
import org.inria.restlet.mta.backend.Requins;
import org.inria.restlet.mta.backend.Zone;
import org.inria.restlet.mta.database.api.Database;

/**
 *
 * Ocean.
 *
 * @author Yoann Le Carrer
 * @author Lamine Sadi
 * 
 */

public class Ocean implements Database {

	static final int MIN_SARDINE = 1;
	static final int MAX_SARDINE = 6;
	static final int MAX_REQUIN = 1;
	static final int MIN_REQUIN = 0;
	static final int MIN_POISSONSPILOTES = 0;
	static final int MAX_POISSONSPILOTES = 5;
	static final int TAILLE_OCEAN = 4;
	static final int NB_REQUINS_MAX = 3;

	static int idZone;
	static int idRequins;
	static int nbTotalPoissonsPilotes = 0;
	static int nbTotalRequins = 0;
	static int nbSardine = 0;
	static int nbPoissonsPilotes = 0;

	Random rand = new Random();

	private Zone[][] zone = new Zone[TAILLE_OCEAN][TAILLE_OCEAN];
	private Poissons_Pilotes[] pilotes = new Poissons_Pilotes[100];
	private Requins[] requins = new Requins[NB_REQUINS_MAX];

	private List<Requins> listeRequins = new ArrayList<Requins>();

	public Ocean() {
		System.out.println("Initialsiaton de l'oc�ans");

		/* Instanciation des zone */
		for (int i = 0; i < TAILLE_OCEAN; i++) {
			for (int j = 0; j < TAILLE_OCEAN; j++) {

				int nbSardine = rand.nextInt(MAX_SARDINE - MIN_SARDINE + 1) + MIN_SARDINE;

				int nbPoissonsPilotes = rand.nextInt(MAX_POISSONSPILOTES - MIN_POISSONSPILOTES + 1)
						+ MIN_POISSONSPILOTES;

				if (nbTotalRequins != NB_REQUINS_MAX) {
					int nbRequins = rand.nextInt(MAX_REQUIN - MIN_REQUIN + 1) + MIN_REQUIN;

						/* Instanciation des Requins */
						for (int r = nbTotalRequins; r < nbTotalRequins + nbRequins; r++) {
							requins[r] = new Requins(zone, i, j);
							requins[r].setIdRequins(idRequins);
							listeRequins.add(requins[r]);
							idRequins++;
							// System.out.println("r n�" + r);
							// System.out.println(requins[r]);
						}
						nbTotalRequins += nbRequins;
					}

				// System.out.println( "nombre de Poisson Pilotes est de : " + nbPoissonsPilotes
				// + " dans la zone :" + i + "," + j);

				// System.out.println("nombre de requins est de : "+ nbRequins + " dans la zone
				// :" + i + "," + j);

				// System.out.println("nombre de sardines est de : " + nbSardine + " dans la
				// zone : " + i + "," + j);

				/* Instanciation des Poisson Pilotes */
				for (int p = nbTotalPoissonsPilotes; p < nbTotalPoissonsPilotes + nbPoissonsPilotes; p++) {
					pilotes[p] = new Poissons_Pilotes(zone, i, j);
					// System.out.println("p n�" + p);
				}

				zone[i][j] = new Zone(i, j, nbSardine);
				zone[i][j].setId(idZone);
				idZone++;

				nbTotalPoissonsPilotes += nbPoissonsPilotes;

			}
		}
	}

	@Override
	public void lancerLaVie() {

		System.out.println("DEBUT");

		System.out.println("Nombre de Requins dans l'oc�an " + nbTotalRequins);

		System.out.println("Nombre de poisson Pilote dans l'oc�an " + nbTotalPoissonsPilotes);

		for (int i = 0; i < nbTotalRequins; i++) {
			// requins[i].setPriority(1+i);
			requins[i].start();
		}

		for (int i = 0; i < nbTotalPoissonsPilotes; i++) {
			// pilotes[i].setDaemon(true);
			pilotes[i].start();
		}

		for (int i = 0; i < nbTotalRequins; i++) {
			try {
				requins[i].join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		for (int i = 0; i < nbTotalPoissonsPilotes; i++) {
			try {
				/*
				 * if (pilotes[i].isAlive()) { pilotes[i].interrupt();
				 * 
				 * }
				 */
				pilotes[i].join();

			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("FINI");
	}

	@Override
	public Zone getZone(int id) {
		for (int i = 0; i < TAILLE_OCEAN; i++) {
			for (int j = 0; j < TAILLE_OCEAN; j++) {
				System.out.println(zone[i][j].getId());
				if (zone[i][j].getId() == id) {
					return zone[i][j];
				}
			}
		}
		return null;
	}

	@Override
	public Requins createRequins() {
		Random rand = new Random();
		int i = rand.nextInt(2 - 0 + 1) + 0;
		int j = rand.nextInt(2 - 0 + 1) + 0;
		while (zone[i][j].getNbRequins()) {
			i = rand.nextInt(2 - 0 + 1) + 0;
			j = rand.nextInt(2 - 0 + 1) + 0;
		}
		Requins requins = new Requins(zone, i, j);
		requins.setIdRequins(idRequins);
		requins.start();
		listeRequins.add(requins);
		idRequins++;
		return requins;
	}

	@Override
	public int getRequins() {
		int requinsEnVie = 0;
		for (int i = 0; i < listeRequins.size(); i++) {
			if (listeRequins.get(i).getNbCycleRequin() > 0) {
				requinsEnVie++;
			}
		}
		return requinsEnVie;
	}

	@Override
	public Requins getRequin(int id) {
		for (int i = 0; i < listeRequins.size(); i++) {
			if (listeRequins.get(i).getIdRequins() == id) {
				return listeRequins.get(i);
			}
		}
		return null;
	}

	@Override
	public int getTunas() {
		int nbSardineTotal = 0;
		for (int i = 0; i < TAILLE_OCEAN; i++) {
			for (int j = 0; j < TAILLE_OCEAN; j++) {
				nbSardineTotal = zone[i][j].getSardine() + nbSardineTotal;
			}
		}
		return nbSardineTotal;
	}

}
