package org.inria.restlet.mta.database.api;

import org.inria.restlet.mta.backend.Requins;
import org.inria.restlet.mta.backend.Zone;

/**
*
* Database.
*
* @author Yoann Le Carrer
* @author Lamine Sadi
* 
*/

public interface Database {

	/**
	 * r�cup�re les information d'une zone
	 *
	 * @param id l'id de la zone
	 * @return la zone concern�
	 */
	Zone getZone(int id);

	/**
	 *
	 * Cr�e un nouveaux requins 
	 *
	 * @return le nouveaux requins.
	 */
	Requins createRequins();
	
	/**
	 *
	 * Returns le nombre de requin encore en vie
	 *
	 * @return le nombre de requins
	 */
	int getRequins();
	
	/**
	 * Retourne le requins poss�dant cette id
	 * 
	 * @param id    l'id du requins
	 * @return le requins concern�
	 */
	Requins getRequin(int id);

	/**
	 *
	 * Retourne le nombre global de sardine
	 *
	 * @return le nombre de sardine restante
	 */
	 int getTunas();
	 
	/**
	 * Permet de lancer tout les Threads
	 * 
	 * @return void
	 */
	 void lancerLaVie();

}
