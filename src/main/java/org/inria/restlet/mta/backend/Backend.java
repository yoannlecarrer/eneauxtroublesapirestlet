package org.inria.restlet.mta.backend;

import org.inria.restlet.mta.database.api.Database;
import org.inria.restlet.mta.database.api.impl.Ocean;

/**
*
* Backend.
*
* @author Yoann Le Carrer
* @author Lamine Sadi
*
*/

public class Backend
{
    private Database database_;

    public Backend()
    {
        database_ = new Ocean();
    }

    public Database getDatabase()
    {
        return database_;
    }

}
