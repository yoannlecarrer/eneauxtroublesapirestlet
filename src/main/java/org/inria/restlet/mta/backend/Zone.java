package org.inria.restlet.mta.backend;

/**
*
* Zone.
*
* @author Yoann Le Carrer
* @author Lamine Sadi
* 
*/

public class Zone {
	
	private int coordoneeX;
	private int coordoneeY;
	private int sardine;
	private int id_;
	private boolean requinsZone = false;
	private Requins requins;

	public Zone(int x, int y, int sardine) {
        this.sardine = sardine;	
        this.coordoneeX = x;
        this.coordoneeY = y; 
    }
	
	/**
	 * D�cr�mente le nombre de sardine de la zone 
	 * 
	 */
	public synchronized void manger() {
		if (sardine > 0 ){
			sardine--;
			System.out.println(Thread.currentThread().getName()+" Mange il reste "+sardine+" sardine sur le site "+coordoneeX+","+coordoneeY);
		}else {
			System.out.println(Thread.currentThread().getName()+" il n'y a plus de sardine dans la zone "+coordoneeX+","+coordoneeY);
		}
    }
	
	/**
	 * Permet � un requins d'entrer dans une zone 
	 * Passe requinZone a true
	 * Si un requins est d�ja dans la zone celui qui veut rentrer doit attendre 
	 * 
	 */
	public synchronized void requinsEntreZone(){
		while(requinsZone == true){
			try {
				System.out.println(Thread.currentThread().getName()+" attend pour rentrer dans la zone : "+coordoneeX+","+coordoneeY);
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println(Thread.currentThread().getName()+" Entre dans la zone : "+coordoneeX+","+coordoneeY);
		setRequinsZone(true);
	
	}
	
	/**
	 * Permet � un requins de quitter la zone  
	 * Passe requinZone a false
	 * R�veille les requins qui attendent
	 * 
	 */
	public synchronized void RequinsSorsZone(){
		setRequinsZone(false);
		System.out.println(Thread.currentThread().getName()+" Sors dans la zone : "+coordoneeX+","+coordoneeY);
		notifyAll();
	}
	
	/**
	 * Avertir les poissons pilote de l'arriver d'un requin 
	 * 
	 */
	public synchronized void avertirPoisonPilote() {
		notifyAll();
	}
	
	/**
	 * Les poissons pilotew attendent qu'un requin entre dans la zone pour �tre reveiller
	 * et ainsi s'attacher � lui 
	 * 
	 */
	public synchronized void attendreRequins(){
		while(requins == null){
			try {
				//System.out.println(Thread.currentThread().getName()+" Attend un requins dans la zone : "+coordoneeX+","+coordoneeY);
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}	
		requins.accroche();
	}
	
	/**
	 * Getter
	 * 
	 */
	public int getSardine() {
		return sardine;
	}
	
	public int getY() {
		return coordoneeY;
	}
	
	public int getX() {
		return coordoneeX;
	}
	
	public boolean getNbRequins() {
		return requinsZone;
	}
	
	public synchronized Requins getRequins() {
		return requins;
	}
	
	public int getId(){
        return id_;
    }
	
	/**
	 * Setter
	 * 
	 */
	public synchronized void setRequinsZone(boolean requinsZone) {
		this.requinsZone = requinsZone;
	}

	public synchronized void setRequins(Requins requins) {
		this.requins = requins;
	}
	
    public void setId(int id){
        id_ = id;
    }
	
}
