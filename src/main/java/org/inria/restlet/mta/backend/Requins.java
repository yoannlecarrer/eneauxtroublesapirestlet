package org.inria.restlet.mta.backend;

import java.util.Random;

/**
*
* Requins.
*
* @author Yoann Le Carrer
* @author Lamine Sadi
* 
*/

public class Requins extends Thread {

	static final int NB_PLACE_MAX = 3;
	private int nbPlace = NB_PLACE_MAX;
	private int nbCycleRequin = 5;
	private int id_;
	private int coordoneeX;
	private int coordoneeY;
	private boolean arrive = false;
	private boolean accrocher = false;
	private Zone[][] zone;

	public Requins(Zone[][] zone, int x, int y) {
		this.zone = zone;
		this.coordoneeX = x;
		this.coordoneeY = y;
	}

	/**
	 * Permet � un Poisson_Pilote de s'acrocher
	 * S�cr�mente le nombre de place si plus de place
	 * les poisson_Pilote attende
	 */
	public synchronized void accroche() {
		while (nbPlace == 0) {
			try {
				System.out.println("Plus de place sur le requins " + this);
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		nbPlace--;
		setAccrocher(true);
		System.out.println(Thread.currentThread().getName() + " c'est accroch� il reste : " + nbPlace
				+ " places sur le requins : " + this);
	}

	/**
	 * Permet � un Poisson_Pilote de se d�crocher
	 * Incr�mente le nombre de place et r�veille les Poisson_Pilote qui attendent
	 * pour s'accrocher au poisson pilote
	 */
	public synchronized void decroche(Zone newZone) {
		// System.out.println("lancienne zone:" +zone[coordoneeX][coordoneeY] +"la
		// nouvelle zone"+ newZone);
		while (zone[coordoneeX][coordoneeY].getRequins() == null || zone[coordoneeX][coordoneeY].equals(newZone)) {
			try {
				System.out.println(Thread.currentThread().getName() + " attend pour decrocher du requins");
				wait();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		nbPlace++;
		System.out.println(Thread.currentThread().getName() + " a lacher il reste : " + nbPlace
				+ " places sur le requins : " + this);
		notifyAll();
	}

	/**
	 * Permet � un requin de manger une sardine
	 */
	public void mangerSardine() {
		zone[coordoneeX][coordoneeY].manger();
	}

	/**
	 * Permet � un requin de quitter �a zone actuel
	 */
	public synchronized void sortirZone() {
		zone[coordoneeX][coordoneeY].RequinsSorsZone();
		zone[coordoneeX][coordoneeY].setRequins(null);
		notifyAll();
	}

	/**
	 * Permet � un requin d'avertir les poissons pilotes de son arriver dans une zone
	 */
	public synchronized void avertirPoisonPiloteRequins() {
		notifyAll();
	}

	/**
	 * Permet � un requin d'entrer dans une nouvelle zone
	 */
	public void entrerZone() {
		Random rand = new Random();
		int direction = rand.nextInt(3 - 0 + 1) + 0;

		if (direction == 0) {
			moveHaut();
		}

		else if (direction == 1) {
			moveGauche();
		}

		else if (direction == 2) {
			moveDroite();
		}

		else if (direction == 3) {
			moveBas();
		}
		zone[coordoneeX][coordoneeY].requinsEntreZone();
		zone[coordoneeX][coordoneeY].setRequins(this);
		setArrive(true);
		this.avertirPoisonPiloteRequins();
		zone[coordoneeX][coordoneeY].avertirPoisonPilote();

	}

	/**
	 * Permet � un requin de se d�placer vers le haut
	 */
	public synchronized void moveHaut() {
		if (coordoneeX == 0) {
			coordoneeX = 3;
		} else {
			coordoneeX--;
		}
	}

	/**
	 * Permet � un requins de se d�placer vers la gauche
	 */
	public synchronized void moveGauche() {
		if (coordoneeY == 0) {
			coordoneeY = 3;
		} else {
			coordoneeY--;
		}
	}

	/**
	 * Permet � un requins de se d�placer vers la droite
	 */
	public synchronized void moveDroite() {
		if (coordoneeY == 3) {
			coordoneeY = 0;
		} else {
			coordoneeY++;
		}
	}

	/**
	 * Permet � un requins de se d�placer vers le bas
	 */
	public synchronized void moveBas() {
		if (coordoneeX == 3) {
			coordoneeX = 0;
		} else {
			coordoneeX++;
		}
	}

	/**
	 * Getter
	 */
	public int getY() {
		return coordoneeY;
	}

	public int getX() {
		return coordoneeX;
	}

	public int getIdRequins() {
		return id_;
	}

	public int getNbCycleRequin() {
		return nbCycleRequin;
	}
	
	public boolean isAccrocher() {
		return accrocher;
	}
	
	public boolean isArrive() {
		return arrive;
	}
	
	/**
	 * Setter
	 */
	public void setNbCycleRequin(int nbCycleRequin_) {
		nbCycleRequin = nbCycleRequin_;
	}
	
	public void setIdRequins(int id) {
		id_ = id;
	}
	
	public void setAccrocher(boolean accrocher) {
		this.accrocher = accrocher;
	}

	public void setArrive(boolean arrive) {
		this.arrive = arrive;
	}

	/**
	 * Permet de lancer le cycle de vie du requin
	 * Ici le requin � un cycle de vie de 5
	 */
	public void run() {
		for (int i = 5; i > 0; i--) {
			sortirZone();
			// Dur�e du d�placement
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			entrerZone();
			// Le temps de manger
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			mangerSardine();
			// Le temps de partir
			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println(this + " , cycle encore de vie " + nbCycleRequin);
			if(i==1) {
				this.decroche(zone[coordoneeX][coordoneeY]);
				this.nbPlace = NB_PLACE_MAX;
				this.setNbCycleRequin(0);
				sortirZone();
				System.out.println(this + " , est mort ");
				return;
			}
			//System.out.println(this + " , cycle encore de vie " + nbCycleRequin);
			this.setNbCycleRequin(i);
		}
	}
}
