package org.inria.restlet.mta.backend;

/**
*
* Poissons_Pilotes.
*
* @author Yoann Le Carrer
* @author Lamine Sadi
* 
*/

public class Poissons_Pilotes extends Thread  {
	
	private int coordoneeX;
	private int coordoneeY;
	private Zone[][] zone;
	private Requins requins;
	
	public Poissons_Pilotes(Zone[][] zone, int x, int y) {
        this.zone = zone;
        this.coordoneeX = x;
        this.coordoneeY = y;
    }
	
	/**
	 * Permet � un poisson_Pilote de s'attacher � un requins
	 */
	public synchronized void attacherRequin() {
		zone[coordoneeX][coordoneeY].attendreRequins();
		setRequins(zone[coordoneeX][coordoneeY].getRequins());
    }
	
	/**
	 * Permet � un poisson_Pilote de lacher un requins
	 */
	public synchronized void lacherRequin() {
		coordoneeX = requins.getX();
		coordoneeY = requins.getY();
		requins.decroche(zone[coordoneeX][coordoneeY]);
		//System.out.println(this+" a lacher");
		setRequins(null);
	}
	
	/**
	 * Retourne le requins sur lequelle le Poisson_Pilote est attach�
	 * @return un requins
	 */
	public Requins getRequins() {
		return requins;
	}

	/**
	 * Permet de donner un requins � un poissonPilote
	 */
	public void setRequins(Requins requins) {
		this.requins = requins;
	}
	
	/**
	 * Permet de lancer le cycle de vie du Poisson_Pilote
	 * ici Le poisson_Pilote � un cycle de vie de 3
	 */
	public void run() {
		while (!Thread.interrupted()) {
			for (int i = 3; i>0; i--) {
				attacherRequin();
				lacherRequin();
			}
		}
	}
}
