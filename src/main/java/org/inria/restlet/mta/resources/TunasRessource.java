package org.inria.restlet.mta.resources;

import org.inria.restlet.mta.backend.Backend;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

/**
*
* TunasRessource.
*
* @author Yoann Le Carrer
* @author Lamine Sadi
* 
*/

public class TunasRessource extends ServerResource {
	
	private Backend backend_;
    
    private int tunas_;;

    public TunasRessource()
    {
    	super();
        backend_ = (Backend) getApplication().getContext().getAttributes()
                .get("backend");
    }
    
    /**
    *
    * R�cup�re le nombre total de sardine restantes
    *
    * @return  JSON representation of the nbSardine restante
    * @throws JSONException   
    *  */
   @Get("json")
   public Representation getTunas() throws JSONException
   {
	   tunas_ = backend_.getDatabase().getTunas();
       JSONObject oceanObject = new JSONObject();
       oceanObject.put("nbSardine", tunas_);
       return new JsonRepresentation(oceanObject);
   }

}
