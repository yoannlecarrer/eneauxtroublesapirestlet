package org.inria.restlet.mta.resources;

import org.inria.restlet.mta.backend.Backend;
import org.inria.restlet.mta.backend.Requins;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

/**
*
* SharkRessource.
*
* @author Yoann Le Carrer
* @author Lamine Sadi
* 
*/

public class SharkRessource extends ServerResource {
	
	private Backend backend_;
	
	private Requins requins_;

    public SharkRessource()
    {
        super();
        backend_ = (Backend) getApplication().getContext().getAttributes().get("backend");
    }
      
    /**
    *
    * r�cuperer la position d�un requin et son temps de vie restant
    *
    * @return JSON representation of Requins
    * @throws JSONException
    */
    @Get("json")
    public Representation getRequin() throws Exception
    {
        String requindString = (String) getRequest().getAttributes().get("sharks_id");
        int requinsId = Integer.valueOf(requindString);
        requins_ = backend_.getDatabase().getRequin(requinsId);

        if(requins_ == null) {
        	JSONObject requinObject = new JSONObject();
        	requinObject.put("requins mort ou n'existe pas", requins_);
        	return new JsonRepresentation(requinObject);
        }else {
        	JSONObject requinObject = new JSONObject();
        	requinObject.put("vie ", requins_.getNbCycleRequin());
        	requinObject.put("zone x ", requins_.getX());
        	requinObject.put("zone y ", requins_.getY());
        	return new JsonRepresentation(requinObject);
        }
    }

}
