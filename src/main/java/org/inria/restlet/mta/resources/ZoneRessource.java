package org.inria.restlet.mta.resources;

import org.inria.restlet.mta.backend.Backend;
import org.inria.restlet.mta.backend.Zone;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

/**
*
* ZoneRessource.
*
* @author Yoann Le Carrer
* @author Lamine Sadi
* 
*/
public class ZoneRessource extends ServerResource {
	
	private Backend backend_;
    
    private Zone zone_;

    public ZoneRessource()
    {
    	super();
    	backend_ = (Backend) getApplication().getContext().getAttributes()
                .get("backend");
    }
    
    /**
    *
    * R�cup�re le nombre total de sardine restantes
    *
    * @return  JSON representation of the nbSardine restante
    * @throws JSONException   
    *  */
    @Get("json")
    public Representation getZone() throws Exception
    {
        String zoneIdString = (String) getRequest().getAttributes().get("zone_id");
        int zoneId = Integer.valueOf(zoneIdString);
        System.out.println();
        zone_ = backend_.getDatabase().getZone(zoneId);

        JSONObject zoneObject = new JSONObject();
        zoneObject.put("nbSardine", zone_.getSardine());
        zoneObject.put("requins", zone_.getNbRequins());

        return new JsonRepresentation(zoneObject);
    }
	
}
