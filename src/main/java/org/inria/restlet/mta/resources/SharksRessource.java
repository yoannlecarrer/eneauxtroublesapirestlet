package org.inria.restlet.mta.resources;

import org.inria.restlet.mta.backend.Backend;
import org.inria.restlet.mta.backend.Requins;
import org.json.JSONException;
import org.json.JSONObject;
import org.restlet.ext.json.JsonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

/**
*
* SharksRessource.
*
* @author Yoann Le Carrer
* @author Lamine Sadi
* 
*/
public class SharksRessource extends ServerResource {
	
	private int requins;
	private Backend backend_;

    public SharksRessource()
    {
    	super();
    	backend_ = (Backend) getApplication().getContext().getAttributes()
                .get("backend");
    }
    
    /**
    *
    * recuperer le nombre de requins encore presents dans la simulation 
    *
    * @return  recuperer le nombre de requins encore presents dans la simulation
    * @throws JSONException
    */
   @Get("json")
   public Representation getRequins() throws JSONException
   {
	   requins = backend_.getDatabase().getRequins();
       JSONObject oceanObject = new JSONObject();
       oceanObject.put("nombre de requins restant", requins);
       return new JsonRepresentation(oceanObject);
   }

   /**
   *
   * Returns requins
   *
   * @return cr�e un nouveau requin dans une zone al�atoire (sans requin)
   * @throws JSONException
   */
   @Post("json")
   public Representation createRequins(JsonRepresentation representation) throws Exception
   {
       Requins requins_ = backend_.getDatabase().createRequins();
       JSONObject resultObject = new JSONObject();
       resultObject.put("idRequins", requins_.getIdRequins());
       JsonRepresentation result = new JsonRepresentation(resultObject);
       return result;
   }

}
