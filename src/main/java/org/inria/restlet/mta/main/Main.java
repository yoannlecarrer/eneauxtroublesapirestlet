package org.inria.restlet.mta.main;

import org.inria.restlet.mta.application.ApplicationOcean;

import org.inria.restlet.mta.backend.Backend;
import org.restlet.Application;
import org.restlet.Component;
import org.restlet.Context;
import org.restlet.data.Protocol;
/**
*
* Main.
*
* @author Yoann Le Carrer
* @author Lamine Sadi
* 
*/

public final class Main
{

    private Main()
    {
        throw new UnsupportedOperationException();
    }

    public static void main(String[] args) throws Exception
    {
        // Create a component
        Component component = new Component();
        Context context = component.getContext().createChildContext();
        component.getServers().add(Protocol.HTTP, 8100);

        // Create an application
        Application application = new ApplicationOcean(context);
        
        // Add the backend into component's context
        Backend backend = new Backend();
        context.getAttributes().put("backend", backend);
        component.getDefaultHost().attach(application);

        // Start the component
        component.start();
        
        // permet de lancer les threads
        backend.getDatabase().lancerLaVie();
    }

}
